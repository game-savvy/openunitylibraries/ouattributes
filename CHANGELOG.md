# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## [0.1.2] - 2020-05-18
### Added
- Expandable Scriptable Objects, its documentation and sample usage


## [0.1.1] - 2020-05-18
### Added
- Import samples button from the Package (package.json updated to include samples)


## [0.1.0] - 2020-05-17
### Added
- This Changelog.
- Empty Editor and Playmode Tests.
- Sample Scene: Containing Examples on how to use the Attributes.

### Changed
- Restructured the project in order to be UnityPackages Complient.

### Removed
- Documents~/images.
