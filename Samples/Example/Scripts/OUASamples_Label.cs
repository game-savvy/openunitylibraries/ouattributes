﻿using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Samples
{
    public class OUASamples_Label : MonoBehaviour
    {

        [Label("Display Name")]
        public float SomeVeryDetailedName = 50f;

    }
}
