﻿using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Samples
{
    public class OUASamples_MinMaxValue : MonoBehaviour
    {

        [MinValue(-2f)]
        [MaxValue(8f)]
        [Tooltip("Value limited between [-2, 8]")]
        public float SomeFloatValue = 0f;
        
    }
}
