﻿using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Samples
{
    public class OUASamples_MinMaxSlider : MonoBehaviour
    {

        [MinMaxSlider(20f, 80f)]
        public Vector2 RandomRange;

    }
}
