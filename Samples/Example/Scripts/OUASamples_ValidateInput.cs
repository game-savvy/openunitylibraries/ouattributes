﻿using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Samples
{
    public class OUASamples_ValidateInput : MonoBehaviour
    {

        [ValidateInput("IsNotNull", "must not be null")]
        public Sprite NotNullSprite;

        private bool IsNotNull(Sprite sprite)
        {
            return sprite != null;
        }
        
    }
}
