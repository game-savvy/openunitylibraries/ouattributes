﻿using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Samples
{
    public class OUASamples_Tag : MonoBehaviour
    {

        [Tag]
        public string SomeTag;

    }
}
