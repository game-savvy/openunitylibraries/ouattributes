﻿using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Samples
{
    public class OUASamples_Required : MonoBehaviour
    {

        [Required]
        public Transform RequiredTransform;

        // Can also pass a custom message to display
        [Required("Must not be null")]
        public GameObject RequiredGameObject;

    }
}
