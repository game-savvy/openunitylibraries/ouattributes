﻿using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Samples
{
    public class OUASamples_ShowHideIf : MonoBehaviour
    {

        [ShowIf("BoolField")]
        public float FirstNumber = 1f;

        [HideIf("NumberGTE3")]
        public float SecondNumber = 2f;

        [ShowIf("TrueFunction")]
        public float ThirdNumber = 0f;

        public bool BoolField = true;

        private bool TrueFunction() => true;

        private bool NumberGTE3() => ThirdNumber >= 3f;


        // You can have multiple conditions

        [ShowIf(EConditionOperator.And, "BoolField", "TrueFunction")]
        public float FourthNumber = 4f;

        [ShowIf(EConditionOperator.Or, "BoolField", "NumberGTE3")]
        public float FiftNumber = 5f;

    }
}
