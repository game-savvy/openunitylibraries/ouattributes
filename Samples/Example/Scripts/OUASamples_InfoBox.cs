﻿using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Samples
{
    public class OUASamples_InfoBox : MonoBehaviour
    {

        [InfoBox("Normal", EInfoBoxType.Normal)]
        public int Int1;

        [InfoBox("Warning", EInfoBoxType.Warning)]
        public int Int2;

        [InfoBox("Error", EInfoBoxType.Error)]
        public int Int3;

    }
}
