﻿using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Samples
{
    public class OUASamples_Scene : MonoBehaviour
    {

        [Scene] //saves the name of the scene
        public string SceneByName_1;

        [Scene(true)] // you can select to show full path
        public string SceneByName_2;

        [Scene] // saves the SceneBuildIndex of the scene
        public int SceneByIndex;

    }
}
