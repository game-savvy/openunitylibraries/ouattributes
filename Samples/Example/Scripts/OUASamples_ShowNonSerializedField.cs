﻿using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Samples
{
    public class OUASamples_ShowNonSerializedField : MonoBehaviour
    {

        [ShowNonSerializedField]
        private int _MyInt = 10;

        [ShowNonSerializedField]
        private const float _PI = 3.14159f;

        [ShowNonSerializedField]
        private static readonly string _SomeString = "Dude, Where's my car?";

        public int SeriazlizedFieldsAtTheTop = 99;

    }
}
