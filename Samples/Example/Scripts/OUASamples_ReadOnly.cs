﻿using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Samples
{
    public class OUASamples_ReadOnly : MonoBehaviour
    {

        public float NotReadOnlyNumber = 100f;

        [ReadOnly]
        public float ReadOnlyNumber = 100f;

    }
}
