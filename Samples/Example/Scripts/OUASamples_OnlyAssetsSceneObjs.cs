﻿using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Samples
{
    public class OUASamples_OnlyAssetsSceneObjs : MonoBehaviour
    {

        [SerializeField]
        [OnlyAsset]
        private GameObject _SomePrefab1;

        [SerializeField]
        [OnlyAsset(true)]
        private GameObject _SomePrefab2;

        [SerializeField]
        [OnlySceneObject]
        private GameObject _SomeSceneObject1;

        [SerializeField]
        [OnlySceneObject(true)]
        private GameObject _SomeSceneObject2;

    }
}
