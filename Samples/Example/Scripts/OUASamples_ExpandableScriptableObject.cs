﻿using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Samples
{

    public class OUASamples_ExpandableScriptableObject : MonoBehaviour
    {

        [MinMaxSlider(20f, 80f)]
        public Vector2 RandomRange;

        [SerializeField]
        private OUASamples_SomeScriptableObject _MySo1;

        [SerializeField]
        private OUASamples_SomeScriptableObject _MySo2;

        [SerializeField, ResizableTextArea]
        private string _SomeLongString;

        [Button]
        private void Foo() { }
    }
}
