﻿using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Samples
{
    public class OUASamples_ProgressBar : MonoBehaviour
    {

        [ProgressBar]
        public float MyProgressBar = 50f;

        [Button]
        private void IncreaseValue() => MyProgressBar += 10.5f;

        [Button]
        private void DecreaseValue() => MyProgressBar -= 12.75f;

    }
}
