﻿using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Samples
{
    public class OUASamples_OnValueChanged : MonoBehaviour
    {

        // Works With Properties

        [SerializeField, OnValueChanged("MyInt")]
        private int _MyInt;
        public int MyInt
        {
            get
            {
                print("Getting");
                return _MyInt;
            }
            set
            {
                print("Setting");
                _MyInt = value;
            }
        }


        [Button]
        private void IncrementProperty()
        {
            MyInt++;
        }

        [Button]
        private void ReadProperty()
        {
            var i = MyInt;
        }

        ////////////////////////
        // Also with Methods

        [SerializeField, OnValueChanged("MyFloatCallback")]
        private float _MyFloat;

        public void MyFloatCallback(float oldVal, float newVal)
        {
            Debug.Log($"Changed MyFloat from [{oldVal}] to [{newVal}]");
        }

    }
}
