﻿using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Samples
{
    public class OUASamples_EnableDisableIf : MonoBehaviour
    {

        [EnableIf("BoolField")]
        public float FirstNumber = 1f;

        [DisableIf("NumberGTE3")]
        public float SecondNumber = 2f;

        [EnableIf("TrueFunction")]
        public float ThirdNumber = 0f;

        public bool BoolField = true;

        private bool TrueFunction() => true;

        private bool NumberGTE3() => ThirdNumber >= 3f;


        // You can have multiple conditions

        [EnableIf(EConditionOperator.And, "BoolField", "TrueFunction")]
        public float FourthNumber = 4f;

        [EnableIf(EConditionOperator.Or, "BoolField", "NumberGTE3")]
        public float FiftNumber = 5f;


    }
}
