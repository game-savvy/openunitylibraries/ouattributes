﻿using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Samples
{
    public class OUASamples_Dropdown : MonoBehaviour
    {

        [Dropdown("_LowPrimes")]
        public int PrimeNumber;
        private int[] _LowPrimes = new int[] { 1, 2, 3, 5, 7, 11, 13, 17, 19 };

        [Dropdown("_Names")]
        public string SelectedName;
        private string[] _Names = new string[] { "Homer", "Marge", "Lisa", "Bart", "Maggie" };

        //Can also use DropdownList<T> Type to provide a readable interface to values
        [Dropdown("_Directions2D")]
        public Vector2 Direction2D;
        private DropdownList<Vector2> _Directions2D = new DropdownList<Vector2>()
        {
            { "North", Vector2.up },
            { "South", Vector2.down },
            { "East", Vector2.right },
            { "West", Vector2.left }
        };

    }
}
