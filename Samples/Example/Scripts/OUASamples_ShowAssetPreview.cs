﻿using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Samples
{
    public class OUASamples_ShowAssetPreview : MonoBehaviour
    {

        [ShowAssetPreview]
        public Sprite SpriteToPreview;

        [ShowAssetPreview]
        public GameObject PrefabToPreview;

        [ShowAssetPreview(256, 256)]
        public GameObject PrefabToPreviewBigger;

    }
}
