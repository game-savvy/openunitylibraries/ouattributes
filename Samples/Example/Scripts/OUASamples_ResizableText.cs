﻿using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Samples
{
    public class OUASamples_ResizableText : MonoBehaviour
    {

        [ResizableTextArea]
        public string LongText;

    }
}
