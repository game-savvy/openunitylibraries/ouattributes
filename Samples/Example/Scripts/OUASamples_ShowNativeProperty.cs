﻿using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Samples
{
    public class OUASamples_ShowNativeProperty : MonoBehaviour
    {

        public readonly float _PI = 3.14159f;
        public float Radius = 0.5f;

        [ShowNativeProperty]
        public float Circumference => 2f * _PI * Radius;

    }
}
