﻿using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Samples
{
    public class OUASamples_BoxGroup : MonoBehaviour
    {

        [BoxGroup]
        public string Player1Name;

        [BoxGroup("Player2")] // It will also include a header title
        public string Player2Name;

        [BoxGroup]
        public int Player1Health;

        [BoxGroup("Player2")] // Groups get re-arranged to stay together
        public int Player2Health;

    }
}
