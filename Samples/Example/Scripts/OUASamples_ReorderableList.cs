﻿using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Samples
{
    public class OUASamples_ReorderableList : MonoBehaviour
    {

        [ReorderableList]
        public int[] Integers;

        [ReorderableList]
        public string[] Strings;

    }
}
