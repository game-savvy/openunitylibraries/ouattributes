﻿using UnityEngine;


namespace GameSavvy.OpenUnityAttributes.Samples
{
    public class OUASamples_Button : MonoBehaviour
    {

        [Button]
        public void PrintHello()
        {
            print("Hello");
        }

        [Button("World")]
        private void PrintWorld()
        {
            print("World");
        }

    }
}
