﻿using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Samples
{

    [CreateAssetMenu]
    public class OUASamples_SomeScriptableObject : ScriptableObject
    {
        public float Health;

        [SerializeField]
        private int _Age;

        [Button]
        private void Talk()
        {
            Debug.Log("Hello World");
        }
    }
}
