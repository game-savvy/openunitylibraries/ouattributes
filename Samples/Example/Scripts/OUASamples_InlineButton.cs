﻿using UnityEngine;


namespace GameSavvy.OpenUnityAttributes.Samples
{
    public class OUASamples_InlineButton : MonoBehaviour
    {
        [InlineButton("ResetVector")]
        public Vector3 VectorToReset;

        [InlineButton("SetThisTransform", "this")]
        public Transform ReferenceTransform;

        private void SetThisTransform()
        {
            ReferenceTransform = transform;
        }

        private void ResetVector()
        {
            VectorToReset = Vector3.zero;
        }

    }
}
