﻿using System;

namespace GameSavvy.OpenUnityAttributes
{
    public enum EInfoBoxType
    {
        Normal,
        Warning,
        Error
    }

    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = true)]
    public class InfoBoxAttribute : DrawerAttribute
    {
        public readonly string Text;
        public readonly EInfoBoxType BoxType;

        public InfoBoxAttribute(string text, EInfoBoxType boxType = EInfoBoxType.Normal)
        {
            Text = text;
            BoxType = boxType;
        }
    }
}
