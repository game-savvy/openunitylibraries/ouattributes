﻿using System;

namespace GameSavvy.OpenUnityAttributes
{
	[AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
	public class AllowNestingAttribute : DrawerAttribute
	{
	}
}
