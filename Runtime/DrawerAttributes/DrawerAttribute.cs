﻿using UnityEngine;

namespace GameSavvy.OpenUnityAttributes
{
	/// <summary>
	/// Base class for all drawer attributes
	/// </summary>
	public class DrawerAttribute : PropertyAttribute, IOUAttribute
	{
	}
}
