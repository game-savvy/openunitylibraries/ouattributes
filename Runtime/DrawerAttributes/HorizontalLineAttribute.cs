﻿using System;

namespace GameSavvy.OpenUnityAttributes
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = true)]
    public class HorizontalLineAttribute : DrawerAttribute
    {
        public const float DefaultHeight = 2.0f;
        public const EColor DefaultColor = EColor.gray;

        public readonly float Height;
        public readonly EColor Color;

        public HorizontalLineAttribute(float height = DefaultHeight, EColor color = DefaultColor)
        {
            Height = height;
            Color = color;
        }
    }
}
