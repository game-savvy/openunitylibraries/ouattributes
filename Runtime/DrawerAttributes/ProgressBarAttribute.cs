﻿using System;

namespace GameSavvy.OpenUnityAttributes
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class ProgressBarAttribute : DrawerAttribute
    {
        public readonly string Name;
        public readonly float MaxValue;
        public readonly EColor Color;

        public ProgressBarAttribute(string name = "", float maxValue = 100, EColor color = EColor.blue)
        {
            Name = name;
            MaxValue = maxValue;
            Color = color;
        }
    }
}
