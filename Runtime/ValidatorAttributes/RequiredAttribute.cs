﻿using System;

namespace GameSavvy.OpenUnityAttributes
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class RequiredAttribute : ValidatorAttribute
    {
        public readonly string Message;

        public RequiredAttribute(string message = null)
        {
            Message = message;
        }
    }
}
