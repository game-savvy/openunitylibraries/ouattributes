﻿using System;

namespace GameSavvy.OpenUnityAttributes
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class OnlySceneObjectAttribute : ValidatorAttribute
    {
        public readonly bool LogToConsole;

        public OnlySceneObjectAttribute(bool logToConsole = false)
        {
            LogToConsole = logToConsole;
        }
    }
}
