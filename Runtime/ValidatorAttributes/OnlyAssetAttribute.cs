﻿using System;

namespace GameSavvy.OpenUnityAttributes
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class OnlyAssetAttribute : ValidatorAttribute
    {
        public readonly bool LogToConsole;

        public OnlyAssetAttribute(bool logToConsole = false)
        {
            LogToConsole = logToConsole;
        }
    }
}
