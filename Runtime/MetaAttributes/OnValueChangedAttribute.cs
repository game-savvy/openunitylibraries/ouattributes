﻿using System;

namespace GameSavvy.OpenUnityAttributes
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = true)]
    public class OnValueChangedAttribute : MetaAttribute
    {
        public readonly string CallbackName;

        /// <summary>
        /// Invokes a callback when the value has changed
        /// The callback can be a method or a property
        /// </summary>
        /// <param name="callbackName">Either the name of a method that receives 2 parameters (T oldValue, T newValue), or the name of a property fo the same type T</param>
        public OnValueChangedAttribute(string callbackName)
        {
            CallbackName = callbackName;
        }
    }
}
