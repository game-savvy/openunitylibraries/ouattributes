﻿using System;

namespace GameSavvy.OpenUnityAttributes
{
    public abstract class EnableIfAttributeBase : MetaAttribute
    {
        public readonly string[] Conditions;
        public readonly EConditionOperator ConditionOperator;
        public bool Inverted { get; protected set; }

        public EnableIfAttributeBase(string condition)
        {
            ConditionOperator = EConditionOperator.And;
            Conditions = new string[1] { condition };
        }

        public EnableIfAttributeBase(EConditionOperator conditionOperator, params string[] conditions)
        {
            ConditionOperator = conditionOperator;
            Conditions = conditions;
        }
    }
}
