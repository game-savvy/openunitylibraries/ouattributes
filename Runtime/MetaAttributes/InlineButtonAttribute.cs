﻿using System;

namespace GameSavvy.OpenUnityAttributes
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class InlineButtonAttribute : MetaAttribute
    {
        public readonly string MethodName;
        public readonly string Label;
        public readonly bool ExpandButton;

        public InlineButtonAttribute(string methodName, string label = null, bool expandButton = false)
        {
            MethodName = methodName;
            Label = label;
            ExpandButton = expandButton;
        }
    }
}
