﻿using System;

namespace GameSavvy.OpenUnityAttributes
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class LabelAttribute : MetaAttribute
    {
        public readonly string Label;

        public LabelAttribute(string label)
        {
            Label = label;
        }
    }
}
