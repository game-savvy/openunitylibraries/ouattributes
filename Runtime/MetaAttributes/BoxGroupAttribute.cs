﻿using System;

namespace GameSavvy.OpenUnityAttributes
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class BoxGroupAttribute : MetaAttribute
    {
        public readonly string Name;

        public BoxGroupAttribute(string name = "")
        {
            Name = name;
        }
    }
}
