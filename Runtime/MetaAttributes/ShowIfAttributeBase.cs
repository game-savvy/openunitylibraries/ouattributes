﻿using System;

namespace GameSavvy.OpenUnityAttributes
{
    public class ShowIfAttributeBase : MetaAttribute
    {
        public readonly string[] Conditions;
        public readonly EConditionOperator ConditionOperator;
        public bool Inverted { get; protected set; }

        public ShowIfAttributeBase(string condition)
        {
            ConditionOperator = EConditionOperator.And;
            Conditions = new string[1] { condition };
        }

        public ShowIfAttributeBase(EConditionOperator conditionOperator, params string[] conditions)
        {
            ConditionOperator = conditionOperator;
            Conditions = conditions;
        }
    }
}
