﻿using System;

namespace GameSavvy.OpenUnityAttributes
{
	public enum EConditionOperator
	{
		And,
		Or
	}
}
