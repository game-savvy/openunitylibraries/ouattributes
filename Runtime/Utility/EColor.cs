﻿using UnityEngine;

namespace GameSavvy.OpenUnityAttributes
{
    public enum EColor
    {
        aliceblue,
        antiquewhite,
        aqua,
        aquamarine,
        azure,
        beige,
        bisque,
        black,
        blanchedalmond,
        blue,
        blueviolet,
        brown,
        burlywood,
        cadetblue,
        chartreuse,
        chocolate,
        coral,
        cornflowerblue,
        cornsilk,
        crimson,
        cyan,
        darkblue,
        darkcyan,
        darkgoldenrod,
        darkgray,
        darkgreen,
        darkgrey,
        darkkhaki,
        darkmagenta,
        darkolivegreen,
        darkorange,
        darkorchid,
        darkred,
        darksalmon,
        darkseagreen,
        darkslateblue,
        darkslategray,
        darkslategrey,
        darkturquoise,
        darkviolet,
        deeppink,
        deepskyblue,
        dimgray,
        dimgrey,
        dodgerblue,
        firebrick,
        floralwhite,
        forestgreen,
        fuchsia,
        gainsboro,
        ghostwhite,
        gold,
        goldenrod,
        gray,
        green,
        greenyellow,
        grey,
        honeydew,
        hotpink,
        indianred,
        indigo,
        ivory,
        khaki,
        lavender,
        lavenderblush,
        lawngreen,
        lemonchiffon,
        lightblue,
        lightcoral,
        lightcyan,
        lightgoldenrodyellow,
        lightgray,
        lightgreen,
        lightgrey,
        lightpink,
        lightsalmon,
        lightseagreen,
        lightskyblue,
        lightslategray,
        lightslategrey,
        lightsteelblue,
        lightyellow,
        lime,
        limegreen,
        linen,
        magenta,
        maroon,
        mediumaquamarine,
        mediumblue,
        mediumorchid,
        mediumpurple,
        mediumseagreen,
        mediumslateblue,
        mediumspringgreen,
        mediumturquoise,
        mediumvioletred,
        midnightblue,
        mintcream,
        mistyrose,
        moccasin,
        navajowhite,
        navy,
        oldlace,
        olive,
        olivedrab,
        orange,
        orangered,
        orchid,
        palegoldenrod,
        palegreen,
        paleturquoise,
        palevioletred,
        papayawhip,
        peachpuff,
        peru,
        pink,
        plum,
        powderblue,
        purple,
        rebeccapurple,
        red,
        rosybrown,
        royalblue,
        saddlebrown,
        salmon,
        sandybrown,
        seagreen,
        seashell,
        sienna,
        silver,
        skyblue,
        slateblue,
        slategray,
        slategrey,
        snow,
        springgreen,
        steelblue,
        tan,
        teal,
        thistle,
        tomato,
        turquoise,
        violet,
        wheat,
        white,
        whitesmoke,
        yellow,
        yellowgreen
    }

    public static class EColorExtensions
    {
        public static Color GetColor(this EColor color)
        {
            switch (color)
            {
                case EColor.aliceblue: return new Color32(240, 248, 255, 255);
                case EColor.antiquewhite: return new Color32(250, 235, 215, 255);
                case EColor.aqua: return new Color32(0, 255, 255, 255);
                case EColor.aquamarine: return new Color32(127, 255, 212, 255);
                case EColor.azure: return new Color32(240, 255, 255, 255);
                case EColor.beige: return new Color32(245, 245, 220, 255);
                case EColor.bisque: return new Color32(255, 228, 196, 255);
                case EColor.black: return Color.black;
                case EColor.blanchedalmond: return new Color32(255, 235, 205, 255);
                case EColor.blue: return new Color32(0, 0, 255, 255);
                case EColor.blueviolet: return new Color32(138, 43, 226, 255);
                case EColor.brown: return new Color32(165, 42, 42, 255);
                case EColor.burlywood: return new Color32(222, 184, 135, 255);
                case EColor.cadetblue: return new Color32(95, 158, 160, 255);
                case EColor.chartreuse: return new Color32(127, 255, 0, 255);
                case EColor.chocolate: return new Color32(210, 105, 30, 255);
                case EColor.coral: return new Color32(255, 127, 80, 255);
                case EColor.cornflowerblue: return new Color32(100, 149, 237, 255);
                case EColor.cornsilk: return new Color32(255, 248, 220, 255);
                case EColor.crimson: return new Color32(220, 20, 60, 255);
                case EColor.cyan: return new Color32(0, 255, 255, 255);
                case EColor.darkblue: return new Color32(0, 0, 139, 255);
                case EColor.darkcyan: return new Color32(0, 139, 139, 255);
                case EColor.darkgoldenrod: return new Color32(184, 134, 11, 255);
                case EColor.darkgray: return new Color32(169, 169, 169, 255);
                case EColor.darkgreen: return new Color32(0, 100, 0, 255);
                case EColor.darkgrey: return new Color32(169, 169, 169, 255);
                case EColor.darkkhaki: return new Color32(189, 183, 107, 255);
                case EColor.darkmagenta: return new Color32(139, 0, 139, 255);
                case EColor.darkolivegreen: return new Color32(85, 107, 47, 255);
                case EColor.darkorange: return new Color32(255, 140, 0, 255);
                case EColor.darkorchid: return new Color32(153, 50, 204, 255);
                case EColor.darkred: return new Color32(139, 0, 0, 255);
                case EColor.darksalmon: return new Color32(233, 150, 122, 255);
                case EColor.darkseagreen: return new Color32(143, 188, 143, 255);
                case EColor.darkslateblue: return new Color32(72, 61, 139, 255);
                case EColor.darkslategray: return new Color32(47, 79, 79, 255);
                case EColor.darkslategrey: return new Color32(47, 79, 79, 255);
                case EColor.darkturquoise: return new Color32(0, 206, 209, 255);
                case EColor.darkviolet: return new Color32(148, 0, 211, 255);
                case EColor.deeppink: return new Color32(255, 20, 147, 255);
                case EColor.deepskyblue: return new Color32(0, 191, 255, 255);
                case EColor.dimgray: return new Color32(105, 105, 105, 255);
                case EColor.dimgrey: return new Color32(105, 105, 105, 255);
                case EColor.dodgerblue: return new Color32(30, 144, 255, 255);
                case EColor.firebrick: return new Color32(178, 34, 34, 255);
                case EColor.floralwhite: return new Color32(255, 250, 240, 255);
                case EColor.forestgreen: return new Color32(34, 139, 34, 255);
                case EColor.fuchsia: return new Color32(255, 0, 255, 255);
                case EColor.gainsboro: return new Color32(220, 220, 220, 255);
                case EColor.ghostwhite: return new Color32(248, 248, 255, 255);
                case EColor.gold: return new Color32(255, 215, 0, 255);
                case EColor.goldenrod: return new Color32(218, 165, 32, 255);
                case EColor.gray: return new Color32(128, 128, 128, 255);
                case EColor.green: return new Color32(0, 128, 0, 255);
                case EColor.greenyellow: return new Color32(173, 255, 47, 255);
                case EColor.grey: return new Color32(128, 128, 128, 255);
                case EColor.honeydew: return new Color32(240, 255, 240, 255);
                case EColor.hotpink: return new Color32(255, 105, 180, 255);
                case EColor.indianred: return new Color32(205, 92, 92, 255);
                case EColor.indigo: return new Color32(75, 0, 130, 255);
                case EColor.ivory: return new Color32(255, 255, 240, 255);
                case EColor.khaki: return new Color32(240, 230, 140, 255);
                case EColor.lavender: return new Color32(230, 230, 250, 255);
                case EColor.lavenderblush: return new Color32(255, 240, 245, 255);
                case EColor.lawngreen: return new Color32(124, 252, 0, 255);
                case EColor.lemonchiffon: return new Color32(255, 250, 205, 255);
                case EColor.lightblue: return new Color32(173, 216, 230, 255);
                case EColor.lightcoral: return new Color32(240, 128, 128, 255);
                case EColor.lightcyan: return new Color32(224, 255, 255, 255);
                case EColor.lightgoldenrodyellow: return new Color32(250, 250, 210, 255);
                case EColor.lightgray: return new Color32(211, 211, 211, 255);
                case EColor.lightgreen: return new Color32(144, 238, 144, 255);
                case EColor.lightgrey: return new Color32(211, 211, 211, 255);
                case EColor.lightpink: return new Color32(255, 182, 193, 255);
                case EColor.lightsalmon: return new Color32(255, 160, 122, 255);
                case EColor.lightseagreen: return new Color32(32, 178, 170, 255);
                case EColor.lightskyblue: return new Color32(135, 206, 250, 255);
                case EColor.lightslategray: return new Color32(119, 136, 153, 255);
                case EColor.lightslategrey: return new Color32(119, 136, 153, 255);
                case EColor.lightsteelblue: return new Color32(176, 196, 222, 255);
                case EColor.lightyellow: return new Color32(255, 255, 224, 255);
                case EColor.lime: return new Color32(0, 255, 0, 255);
                case EColor.limegreen: return new Color32(50, 205, 50, 255);
                case EColor.linen: return new Color32(250, 240, 230, 255);
                case EColor.magenta: return new Color32(255, 0, 255, 255);
                case EColor.maroon: return new Color32(128, 0, 0, 255);
                case EColor.mediumaquamarine: return new Color32(102, 205, 170, 255);
                case EColor.mediumblue: return new Color32(0, 0, 205, 255);
                case EColor.mediumorchid: return new Color32(186, 85, 211, 255);
                case EColor.mediumpurple: return new Color32(147, 112, 219, 255);
                case EColor.mediumseagreen: return new Color32(60, 179, 113, 255);
                case EColor.mediumslateblue: return new Color32(123, 104, 238, 255);
                case EColor.mediumspringgreen: return new Color32(0, 250, 154, 255);
                case EColor.mediumturquoise: return new Color32(72, 209, 204, 255);
                case EColor.mediumvioletred: return new Color32(199, 21, 133, 255);
                case EColor.midnightblue: return new Color32(25, 25, 112, 255);
                case EColor.mintcream: return new Color32(245, 255, 250, 255);
                case EColor.mistyrose: return new Color32(255, 228, 225, 255);
                case EColor.moccasin: return new Color32(255, 228, 181, 255);
                case EColor.navajowhite: return new Color32(255, 222, 173, 255);
                case EColor.navy: return new Color32(0, 0, 128, 255);
                case EColor.oldlace: return new Color32(253, 245, 230, 255);
                case EColor.olive: return new Color32(128, 128, 0, 255);
                case EColor.olivedrab: return new Color32(107, 142, 35, 255);
                case EColor.orange: return new Color32(255, 165, 0, 255);
                case EColor.orangered: return new Color32(255, 69, 0, 255);
                case EColor.orchid: return new Color32(218, 112, 214, 255);
                case EColor.palegoldenrod: return new Color32(238, 232, 170, 255);
                case EColor.palegreen: return new Color32(152, 251, 152, 255);
                case EColor.paleturquoise: return new Color32(175, 238, 238, 255);
                case EColor.palevioletred: return new Color32(219, 112, 147, 255);
                case EColor.papayawhip: return new Color32(255, 239, 213, 255);
                case EColor.peachpuff: return new Color32(255, 218, 185, 255);
                case EColor.peru: return new Color32(205, 133, 63, 255);
                case EColor.pink: return new Color32(255, 192, 203, 255);
                case EColor.plum: return new Color32(221, 160, 221, 255);
                case EColor.powderblue: return new Color32(176, 224, 230, 255);
                case EColor.purple: return new Color32(128, 0, 128, 255);
                case EColor.rebeccapurple: return new Color32(102, 51, 153, 255);
                case EColor.red: return new Color32(255, 0, 0, 255);
                case EColor.rosybrown: return new Color32(188, 143, 143, 255);
                case EColor.royalblue: return new Color32(65, 105, 225, 255);
                case EColor.saddlebrown: return new Color32(139, 69, 19, 255);
                case EColor.salmon: return new Color32(250, 128, 114, 255);
                case EColor.sandybrown: return new Color32(244, 164, 96, 255);
                case EColor.seagreen: return new Color32(46, 139, 87, 255);
                case EColor.seashell: return new Color32(255, 245, 238, 255);
                case EColor.sienna: return new Color32(160, 82, 45, 255);
                case EColor.silver: return new Color32(192, 192, 192, 255);
                case EColor.skyblue: return new Color32(135, 206, 235, 255);
                case EColor.slateblue: return new Color32(106, 90, 205, 255);
                case EColor.slategray: return new Color32(112, 128, 144, 255);
                case EColor.slategrey: return new Color32(112, 128, 144, 255);
                case EColor.snow: return new Color32(255, 250, 250, 255);
                case EColor.springgreen: return new Color32(0, 255, 127, 255);
                case EColor.steelblue: return new Color32(70, 130, 180, 255);
                case EColor.tan: return new Color32(210, 180, 140, 255);
                case EColor.teal: return new Color32(0, 128, 128, 255);
                case EColor.thistle: return new Color32(216, 191, 216, 255);
                case EColor.tomato: return new Color32(255, 99, 71, 255);
                case EColor.turquoise: return new Color32(64, 224, 208, 255);
                case EColor.violet: return new Color32(238, 130, 238, 255);
                case EColor.wheat: return new Color32(245, 222, 179, 255);
                case EColor.whitesmoke: return new Color32(245, 245, 245, 255);
                case EColor.yellow: return new Color32(255, 255, 0, 255);
                case EColor.yellowgreen: return new Color32(154, 205, 50, 255);
                case EColor.white:
                default: return Color.white;
            }
        }
    }
}
