## DEPRECATED
For top quality attributes use Odin Inspector  
You can also use Naughty Attributes as another alternative

# Open Unity Attributes
Open Unity Attributes (OUA) is an extension for the Unity Inspector originaly a fork from [Naughty Attributes][LinkNaughtyAttributes].

It expands the range of attributes that Unity provides so that you can create powerful inspectors without the need of custom editors or property drawers. It also provides attributes that can be applied to non-serialized fields or functions.

It is implemented by replacing the default Unity Inspector. This means that if you have any custom editors, **Open Unity Attributes** will not work with them (**This is planned to change in the near future**).  
All of your custom editors and property drawers are not affected in any way.
  
___
## How to Install

### **With the GitUrl**
1. First, open the **Package Manager Window**  
![Open Package Manager](https://i.imgur.com/nWdJTFt.png)

2. Second, on the top left **+** sign, select Add Package from git URL...  
![PackageManager Add from Git URL](https://i.imgur.com/pC2m4pz.png)

3. Third, paste The ***HTTPS GitURL*** in the text field

The library is ready to be used...  
  
### **Through the Manifest.json**
You can also install via git url by adding this entry in your **manifest.json**
```
"com.game-savvy.ouattributes": "https://gitlab.com/game-savvy/openunitylibraries/ouattributes.git"
```
After this , you will be able to find the Package in the **Package Manager** window.
___
## System Requirements
Unity 2019.2.0f1 or later versions.

___
## Dependencies
***No Dependencies**
  
>While this Library does not depend on other Libraries, it is itself a requirement for other libraries such as  
>- [*OUTools*][LinkOUTools]
>- [*OUSscriptableLibrary*][LinkOUSscriptableLibrary]

___
## Drawer Attributes
### Provide special draw options to serialized fields.
  
### **Button**
A method can be marked as a button. A button appears in the inspector and executes the method if clicked.  
Works both with instance and static methods.

``` csharp
    [Button]
    private void PrintHello()
    {
        print("Hello");
    }

    [Button("World")]
    private void PrintWorld()
    {
        print("World");
    }
```
![ButtonImage](https://media.giphy.com/media/iiWAavpoFVE1Xf87FQ/giphy.gif)  
  
### **InlineButton**
Draws a button to the right of the Field/Property. Calls method from provided method name.

``` csharp
    [InlineButton("ResetVector")]
    public Vector3 VectorToReset;

    [InlineButton("SetThisTransform", "this")]
    public Transform ReferenceTransform;

    private void SetThisTransform()
    {
        ReferenceTransform = transform;
    }

    private void ResetVector()
    {
        VectorToReset = Vector3.zero;
    }
```
![InlineButtonImage](https://media.giphy.com/media/QxSaKAQPD0RFP10i4D/giphy.gif)  
    
### **MinValue and MaxValue**
Used for limiting the minimum and maximum Values of a field when changed in the inspector.  
Valid for Int and Float types.  
A field can have infinite number of validator attributes

``` csharp
    [MinValue(-2f)]
    [MaxValue(8f)]
    public float SomeFloatValue = 0f;
```
![MinAndMaxImage](https://media.giphy.com/media/Ihga7x9h0S8l9b1qIa/giphy.gif)  
  
### **ValidateInput**
Can be used to Validate if a field complies with some properties, if not, then a messagebox is shown as an error or warning.
Conditions can be Fields, Properties or function, in general anything that can evaluate to true or false.

``` csharp
    [ValidateInput("IsNotNull", "must not be null")]
    public Sprite NotNullSprite;

    private bool IsNotNull(Sprite sprite)
    {
        return sprite != null;
    }
```
![ValidateInputGif](https://media.giphy.com/media/hQQeRUjuEEFoZ2yfry/giphy.gif)  
  
### **EnableIf / DisableIf**
Enables or disables the Field in the inspector if the return value of the condition evaluates to true respectively.  
Conditions can be Fields or function, in general anything that can evaluate to true or false.

``` csharp
    [EnableIf("BoolField")]
    public float FirstNumber = 1f;

    [DisableIf("NumberGTE3")]
    public float SecondNumber = 2f;

    [EnableIf("TrueFunction")]
    public float ThirdNumber = 0f;

    public bool BoolField = true;

    private bool TrueFunction() => true;

    private bool NumberGTE3() => ThirdNumber >= 3f;


    // You can have multiple conditions

    [EnableIf(EConditionOperator.And, "BoolField", "TrueFunction")]
    public float FourthNumber = 4f;

    [EnableIf(EConditionOperator.Or, "BoolField", "NumberGTE3")]
    public float FiftNumber = 5f;
```
![EnableIfDisableIfGif](https://media.giphy.com/media/QWRAOFSh18z9O5Vj1t/giphy.gif) 
  
### **ShowIf / HideIf**
Shows or Hides the Field in the inspector if the return value of the condition evaluates to true respectively.  
Conditions can be Fields, Properties or function, in general anything that can evaluate to true or false.

``` csharp
    [ShowIf("BoolField")]
    public float FirstNumber = 1f;

    [HideIf("NumberGTE3")]
    public float SecondNumber = 2f;

    [ShowIf("TrueFunction")]
    public float ThirdNumber = 0f;

    public bool BoolField = true;

    private bool TrueFunction() => true;

    private bool NumberGTE3() => ThirdNumber >= 3f;


    // You can have multiple conditions

    [ShowIf(EConditionOperator.And, "BoolField", "TrueFunction")]
    public float FourthNumber = 4f;

    [ShowIf(EConditionOperator.Or, "BoolField", "NumberGTE3")]
    public float FiftNumber = 5f;
```
![ShowIfHideIfGif](https://media.giphy.com/media/cKVRvpZtUPSkbbzJ5M/giphy.gif)  

### **MinMaxSlider**
A double slider. The **min value** is saved to the **X** property, and the **max value** is saved to the **Y** property of a **Vector2** field.

``` csharp
    [MinMaxSlider(20f, 80f)]
    public Vector2 RandomRange;
```
![MinMaxSliderImage](https://media.giphy.com/media/dzC469nMwrQvlZQo8g/giphy.gif)  
  
### **ProgressBar**
Simply displays a progress bar instead of a normal number field

``` csharp
    [ProgressBar]
    public float MyProgressBar = 50f;

    [Button]
    private void IncreaseValue() => MyProgressBar += 10.5f;

    [Button]
    private void DecreaseValue() => MyProgressBar -= 12.75f;
```
![ProgressBarImage](https://media.giphy.com/media/lnDhAlcyKLRzNTWMm2/giphy.gif)  
  
### **Label**
Overrides the field's default label

``` csharp
    [Label("Display Name")]
    public float SomeVeryDetailedName = 50f;
```
![LabelImage](https://media.giphy.com/media/U4MZ4r2nwpt3RMlo5b/giphy.gif)  
  
### **OnValueChanged**
Triggers a Get and Set calls in a Property from a modifying a Field in the Inspector or a method call with 2 parameters (oldValue, newValue)

``` csharp
    ////////////////////////
    // Works With Properties

    [SerializeField, OnValueChanged("MyInt")]
    private int _MyInt;
    public int MyInt
    {
        get
        {
            print("Getting");
            return _MyInt;
        }
        set
        {
            print("Setting");
            _MyInt = value;
        }
    }


    [Button]
    private void IncrementProperty()
    {
        MyInt++;
    }

    [Button]
    private void ReadProperty()
    {
        var i = MyInt;
    }

    ////////////////////////
    // Also with Methods

    [SerializeField, OnValueChanged("MyFloatCallback")]
    private float _MyFloat;

    public void MyFloatCallback(float oldVal, float newVal)
    {
        Debug.Log($"Changed MyFloat from [{oldVal}] to [{newVal}]");
    }
```
![GetSetImage](https://media.giphy.com/media/ZCThrZ0FGNkNKRQaMg/giphy.gif)  
  
### **Dropdown**
Displays a dropdown list from the values in the provided container.

``` csharp
    [Dropdown("_LowPrimes")]
    public int PrimeNumber;
    private int[] _LowPrimes =  new int[] {1,2,3,5,7,11,13,17,19};

    [Dropdown("_Names")]
    public string SelectedName;
    private string[] _Names =  new string[] {"Homer", "Marge", "Lisa", "Bart", "Maggie"};

    //Can also use DropdownList<T> Type to provide a readable interface to values
    [Dropdown("_Directions2D")]
    public Vector2 Direction2D;
    private DropdownList<Vector2> _Directions2D = new DropdownList<Vector2>()
    {
        { "North", Vector2.up },
        { "South", Vector2.down },
        { "East", Vector2.right },
        { "West", Vector2.left }
    };
```
![DorpdownImage](https://media.giphy.com/media/MEe3m8CBCQdX4zg4Ce/giphy.gif)  
  
### **Tag**
Enable Tag selection with string field

``` csharp
    [Tag]
    public string SomeTag;
```
![TagImage](https://media.giphy.com/media/Ii4nyXaI7NE86Ql5JF/giphy.gif)  
  
### **Scene**
Enable Scene selection with string or int fields.  
**NOTE:** *Scenes must be in the SceneBuild settings*.  

``` csharp
    [Scene] //saves the name of the scene
    public string SceneByName_1;
    
    [Scene(true)] // you can select to show full path
    public string SceneByName_2;

    [Scene] // saves the SceneBuildIndex of the scene
    public int SceneByIndex;
```
![SceneImage](https://media.giphy.com/media/SRwcmZYnbxUz86cER7/giphy.gif)  
  
### **Required**
Displays an error message if the reference to this object is null/missing/empty, Can also Log to console.  
This also works when using the Ref Validation tool from OpenUnityScriptableLibrary (Ctrl+Alt+Shift+V)

``` csharp
    [Required]
    public Transform RequiredTransform;

    // Can also pass a custom message to display
    [Required("Must not be null")]
    public GameObject RequiredGameObject;
```
![RequiredImage](https://media.giphy.com/media/ZCYynVz4P2L0hp08N2/giphy.gif)  
  
### **OnlyAssets And OnlySceneObjects**
Can only select either assets in the project, or objects in the scene.  
This also forces the select tool to filter shown objects.

``` csharp
    [SerializeField]
    [OnlyAsset]
    private GameObject _SomePrefab1;

    [SerializeField]
    [OnlyAsset(true)]
    private GameObject _SomePrefab2;

    [SerializeField]
    [OnlySceneObject]
    private GameObject _SomeSceneObject1;

    [SerializeField]
    [OnlySceneObject(true)]
    private GameObject _SomeSceneObject2;
```
![OnlyAssetsAndOnlySceneObjectsImage](https://media.giphy.com/media/MEROS4OpSNnypDDtyM/giphy.gif)  
  
### **ResizableTextArea**
A dynamically resizable text area where you can see the whole text.

``` csharp
    [ResizableTextArea]
    public string LongText;
```
![ResizableTextAreaImage](https://media.giphy.com/media/ZCGiIRQ2IOWyFrw1Al/giphy.gif)  
  
### **ReorderableList**
Provides array type fields with an interface for easy reordering of elements.

``` csharp
    [ReorderableList]
    public int[] Integers;

    [ReorderableList]
    public string[] Strings;
```
![ReorderableListGIF](https://media.giphy.com/media/kEp2Ft0pDPgvo7EsGf/giphy.gif)  
  
### **ShowAssetPreview**
Shows the texture preview of a given asset (Sprite, Prefab, etc...)

``` csharp
    [ShowAssetPreview]
    public Sprite SpriteToPreview;

    [ShowAssetPreview]
    public GameObject PrefabToPreview;

    [ShowAssetPreview(256, 256)]
    public GameObject PrefabToPreviewBigger;
```
![ShowAssetPreviewImage](https://media.giphy.com/media/d7g8AjTsRJXhDAY7vj/giphy.gif)  
  
### **ReadOnly**
Makes a serialized field read only.

``` csharp
    public float NotReadOnlyNumber = 100f;

    [ReadOnly]
    public float ReadOnlyNumber = 100f;
```
![ReadOnlyImage](https://media.giphy.com/media/lOxro2CUrmb9GJMTgu/giphy.gif) 
  
### **ShowNonSerializedField**
Shows non-serialized fields in the inspector.
All non-serialized fields are displayed at the bottom of the inspector before the method buttons.  
If you change a *non-static non-serialized* field in the code - the value in the inspector will be updated after you press **Play** in the editor.  
On the other hand *static non-serialized* fields values are updated at compile time.  
It supports only certain types **(bool, int, long, float, double, string, Vector2, Vector3, Vector4, Color, Bounds, Rect, UnityEngine.Object)**.

``` csharp
    [ShowNonSerializedField]
    private int _MyInt = 10;

    [ShowNonSerializedField]
    private const float _PI = 3.14159f;

    [ShowNonSerializedField]
    private static readonly string _SomeString = "Dude, Where's my car?";

    public int SeriazlizedFieldsAtTheTop = 99;
```
![ShowNonSerializedFieldImage](https://media.giphy.com/media/Rlr89NeIq8ty0Kb6On/giphy.gif)  
  
### **ShowNativeProperty**
Shows native C# properties in the inspector.  
All native properties are displayed at the bottom of the inspector after the non-serialized fields and before the method buttons.  
It supports only certain types **(bool, int, long, float, double, string, Vector2, Vector3, Vector4, Color, Bounds, Rect, UnityEngine.Object)**.

``` csharp
    public readonly float _PI = 3.14159f;
    public float Radius = 0.5f;

    [ShowNativeProperty]
    public float Circumference => 2f * _PI * Radius;
```
![ShowNativePropertyImage](https://media.giphy.com/media/YOkX8raAZ3UIJHPw6B/giphy.gif)  
  
### **BoxGroup**
Groups fields and displays them within a Grouped box

``` csharp
    [BoxGroup]
    public string Player1Name;

    [BoxGroup("Player2")] // It will also include a header title
    public string Player2Name;

    [BoxGroup]
    public int Player1Health;

    [BoxGroup("Player2")] // Groups get re-arranged to stay together
    public int Player2Health;
```
![BoxGroupImage](https://i.imgur.com/7wDtLTi.png)  
  
### **InfoBox**
Displays an info box in the inspector before the normal display of the field

``` csharp
    [InfoBox("Normal", EInfoBoxType.Normal)]
    public int Int1;

    [InfoBox("Warning", EInfoBoxType.Warning)]
    public int Int2;

    [InfoBox("Error", EInfoBoxType.Error)]
    public int Int3;
```
![InfoBoxImage](https://i.imgur.com/A6JWCeR.png)  
  
### **Expandable ScriptableObjects**
Not an Attribute, simply allows Scriptable Objects to be expanded into the same Inspector window.  
No need to click on the SO itself in order to see its inspector.

``` csharp
    // Works with cusutom inspector and other attributes
    [MinMaxSlider(20f, 80f)]
    public Vector2 RandomRange;

    [SerializeField]
    private OUASamples_SomeScriptableObject _MySo1;

    [SerializeField]
    private OUASamples_SomeScriptableObject _MySo2;

    [SerializeField, ResizableTextArea]
    private string _SomeLongString;

    // Also works with Buttons, and everything OUAttribtues
    [Button]
    private void Foo() { }
```
![ExpandableSOImage](https://media.giphy.com/media/kfRFjYOacyfeTSifbD/source.gif)  
  
---
## **The future of Open Unity Attributes**
Besides keep adding new attributes to the library, there are some things that i have planned for the future of it.

1. Refactor the code so it no longer requires to generate code.  
By doing this it will be able to play better with already existing Unity Attributes, so we wil be able to combine/Mix&Match them.

2. Refactor each attribute so all Attributes will eventually use UIElemets.  
This will allow for better performance, custom theming and better maintainability since Unity will be using UIElements for the editor in the future.
  
If you have anything that you would like to add/see/change in this library, please feel free to contribute to it.
  
___
## How to create your own attributes *(V1)*
> keep in mind that how Attributes are implemented is bound to change in the newar future, however dont let this stop you from contributting to the library, i will take care of the refactoring of the entire library to use an undated CORE and eventually to UIElements

Lets say you want to implement your own **[ReadOnly]** attribute.

First you have to create a **ReadOnlyAttribute** class
``` csharp
[AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
public class ReadOnlyAttribute : DrawerAttribute
{
    //If required to have some field per Attribute, make them **readonly** unless necessary
    public readonly string SomeRelevantParameter;
}
```

Then you need to create a drawer for that attribute
``` csharp
[PropertyDrawer(typeof(ReadOnlyAttribute))]
public class ReadOnlyPropertyDrawer : PropertyDrawer
{
	public override void DrawProperty(SerializedProperty property)
	{
		var readOnlyAttribute = property.GetAttribute<ReadOnlyAttribute>(); //This is how you get a reference to the actual Attribute
		var relevantParameter = readOnlyAttribute.SomeRelevantParameter; //This is how you use the Attributes "parameters"/Fields

		GUI.enabled = false;
		EditorGUILayout.PropertyField(property, true);
		GUI.enabled = true;
	}
}
```

Last, in order for the editor to recognize the drawer for this attribute, you have to press the **Tools/OpenUnityAttribuets/Update Attributes Database** menu item in the editor.

### ***Want to Help? How to contribute to this Repo***

For all code, please follow these [Coding Standards and Naming Conventions][LinkUnityCodingStandards].  
Simply work on your fork, create a feature branch and when done please submit a pull request.
  
___
## **License**
MIT License

Copyright (c) 2020 Willy Campos

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

[LinkNaughtyAttributes]: https://github.com/dbrizov/NaughtyAttributes
[LinkOUTools]: https://gitlab.com/game-savvy/openunitylibraries/outools
[LinkOUSscriptableLibrary]: https://gitlab.com/game-savvy/openunitylibraries/ousl
[LinkUnityCodingStandards]: https://gitlab.com/wcampospro/CodingConvention_Unity
