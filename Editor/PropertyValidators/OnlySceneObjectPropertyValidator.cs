﻿using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    public class OnlySceneObjectPropertyValidator : PropertyValidatorBase
    {
        public override void ValidateProperty(SerializedProperty property)
        {
            var onlySceneObjectAttribute = PropertyUtility.GetAttribute<OnlySceneObjectAttribute>(property);
            bool logToConsole = onlySceneObjectAttribute.LogToConsole;

            var targetObject = property.serializedObject.targetObject;
            var targetObjectClassType = targetObject.GetType();
            var field = targetObjectClassType.GetField(property.propertyPath, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            string error = property.displayName + " Must Be a Scene Object not an Asset";

            if (field != null)
            {
                var value = field.GetValue(targetObject);
                if (value != null && value.ToString() != "null" && AssetDatabase.Contains(value as Object))
                {
                    OUAEditorGUI.HelpBox_Layout(error, MessageType.Error, context: property.serializedObject.targetObject, logToConsole: logToConsole);
                }
            }
            // field.SetValue
            // (
            //     targetObject,
            //     EditorGUILayout.ObjectField(property.displayName, field.GetValue(targetObject) as Object, field.FieldType, true)
            // );
        }
    }
}
