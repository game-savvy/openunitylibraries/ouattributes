﻿using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    public class OnlyAssetPropertyValidator : PropertyValidatorBase
    {
        public override void ValidateProperty(SerializedProperty property)
        {
            var onlyAssetsAttribute = PropertyUtility.GetAttribute<OnlyAssetAttribute>(property);
            bool logToConsole = onlyAssetsAttribute.LogToConsole;

            var targetObject = property.serializedObject.targetObject as object;
            var targetObjectClassType = targetObject.GetType();
            var field = targetObjectClassType.GetField(property.propertyPath, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            string error = property.displayName + " Must Be an Asset";

            if (field != null)
            {
                var value = field.GetValue(targetObject);
                if (value != null && value.ToString() != "null" && AssetDatabase.Contains(value as Object) == false)
                {
                    OUAEditorGUI.HelpBox_Layout(error, MessageType.Error, context: property.serializedObject.targetObject, logToConsole: logToConsole);
                }
            }
        }
    }
}
