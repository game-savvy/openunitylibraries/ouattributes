﻿using System.Collections.Generic;
using System.IO;
using UnityEditor;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    public static class SceneUtilities
    {
        public static List<string> GetScenesInBuild(bool showPath)
        {
            List<string> allScenes = new List<string>();
            foreach (var scene in EditorBuildSettings.scenes)
            {
                var fileName = scene.path;
                if (showPath == false)
                {
                    fileName = Path.GetFileName(scene.path);
                    fileName = fileName.Substring(0, fileName.IndexOf("."));
                }
                allScenes.Add(fileName);
            }

            return allScenes;
        }
    }
}
