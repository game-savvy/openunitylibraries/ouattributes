using UnityEngine;
using UnityEditor;
using System.Reflection;
using System.Linq;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    public static class OUAEditorGUI
    {
        public static void PropertyField_Layout(SerializedProperty property, bool includeChildren)
        {
            SpecialCaseDrawerAttribute specialCaseAttribute = PropertyUtility.GetAttribute<SpecialCaseDrawerAttribute>(property);
            if (specialCaseAttribute != null)
            {
                specialCaseAttribute.GetDrawer().OnGUI(property);
            }
            else
            {
                GUIContent label = new GUIContent(PropertyUtility.GetLabel(property));
                bool anyDrawerAttribute = PropertyUtility.GetAttributes<DrawerAttribute>(property).Any();

                if (anyDrawerAttribute == false)
                {
                    // Drawer attributes check for visibility, enableability and validator themselves,
                    // so if a property doesn't have a DrawerAttribute we need to check for these explicitly

                    bool visible = PropertyUtility.IsVisible(property);
                    if (visible == false)
                    {
                        return;
                    }

                    bool validatorTypeOf_Only_X = false;
                    ValidatorAttribute[] validatorAttributes = PropertyUtility.GetAttributes<ValidatorAttribute>(property);
                    foreach (var validatorAttribute in validatorAttributes)
                    {
                        if (validatorAttribute.GetType() == typeof(OnlyAssetAttribute))
                        {
                            validatorTypeOf_Only_X = true;
                        }
                        validatorAttribute.GetValidator().ValidateProperty(property);
                    }

                    EditorGUI.BeginChangeCheck();
                    bool enabled = PropertyUtility.IsEnabled(property);
                    GUI.enabled = enabled;

                    var inlineButtonAttribute = PropertyUtility.GetAttribute<InlineButtonAttribute>(property);
                    if (inlineButtonAttribute == null)
                    {
                        if (validatorTypeOf_Only_X)
                        {
                            var targetObject = property.serializedObject.targetObject as object;
                            var targetObjectClassType = targetObject.GetType();
                            var field = targetObjectClassType.GetField(property.propertyPath, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                            field.SetValue
                            (
                                targetObject,
                                EditorGUILayout.ObjectField(label, field.GetValue(targetObject) as UnityEngine.Object, field.FieldType, false)
                            );
                        }
                        else
                        {
                            EditorGUILayout.PropertyField(property, label, includeChildren);
                        }
                    }
                    else
                    {
                        DrawPropertyFieldWithInlineButton(property, inlineButtonAttribute, validatorTypeOf_Only_X);
                    }
                    GUI.enabled = true;

                    if (EditorGUI.EndChangeCheck()) // Call OnValueChanged callbacks
                    {
                        PropertyUtility.CallOnValueChangedCallbacks(property);
                    }
                }
                else
                {
                    EditorGUILayout.PropertyField(property, label, includeChildren);
                }
            }
        }

        public static void DrawPropertyFieldWithInlineButton(SerializedProperty property, InlineButtonAttribute inlineButtonAttribute, bool objectField = false)
        {
            var targetObject = property.serializedObject.targetObject;

            var methodInfo = ReflectionUtility.GetMethod(targetObject, inlineButtonAttribute.MethodName);//parentObject.GetMethod(inlineButtonAttribute.MethodName); //, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            var displayName = inlineButtonAttribute.Label == null ? inlineButtonAttribute.MethodName : inlineButtonAttribute.Label;

            var guiContent = new GUIContent(property.displayName);
            EditorGUILayout.BeginHorizontal();
            {
                if (objectField)
                {
                    var targetObjectClassType = targetObject.GetType();
                    var field = targetObjectClassType.GetField(property.propertyPath, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                    field.SetValue
                    (
                        targetObject,
                        EditorGUILayout.ObjectField(PropertyUtility.GetLabel(property), field.GetValue(targetObject) as UnityEngine.Object, field.FieldType, false)
                    );
                }
                else
                {
                    EditorGUILayout.PropertyField(property, guiContent, true);
                }

                if (GUILayout.Button(displayName, GUILayout.ExpandWidth(inlineButtonAttribute.ExpandButton)))
                {
                    methodInfo.Invoke(targetObject, null);
                }
            }
            EditorGUILayout.EndHorizontal();
        }

        public static float GetIndentLength(Rect sourceRect)
        {
            Rect indentRect = EditorGUI.IndentedRect(sourceRect);
            float indentLength = indentRect.x - sourceRect.x;

            return indentLength;
        }

        public static void BeginBoxGroup_Layout(string label = "")
        {
            EditorGUILayout.BeginVertical(GUI.skin.box);
            if (string.IsNullOrEmpty(label) == false)
            {
                EditorGUILayout.LabelField(label, EditorStyles.boldLabel);
            }
        }

        public static void EndBoxGroup_Layout()
        {
            EditorGUILayout.EndVertical();
        }

        /// <summary>
        /// Creates a dropdown
        /// </summary>
        /// <param name="rect">The rect the defines the position and size of the dropdown in the inspector</param>
        /// <param name="serializedObject">The serialized object that is being updated</param>
        /// <param name="target">The target object that contains the dropdown</param>
        /// <param name="dropdownField">The field of the target object that holds the currently selected dropdown value</param>
        /// <param name="label">The label of the dropdown</param>
        /// <param name="selectedValueIndex">The index of the value from the values array</param>
        /// <param name="values">The values of the dropdown</param>
        /// <param name="displayOptions">The display options for the values</param>
        public static void Dropdown(
            Rect rect, SerializedObject serializedObject, object target, FieldInfo dropdownField,
            string label, int selectedValueIndex, object[] values, string[] displayOptions)
        {
            EditorGUI.BeginChangeCheck();

            int newIndex = EditorGUI.Popup(rect, label, selectedValueIndex, displayOptions);

            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(serializedObject.targetObject, "Dropdown");

                // _TODO: Problem with structs, because they are value type.
                // The solution is to make boxing/unboxing but unfortunately I don't know the compile time type of the target object
                dropdownField.SetValue(target, values[newIndex]);
            }
        }

        public static void Button(UnityEngine.Object target, MethodInfo methodInfo)
        {
            if (methodInfo.GetParameters().Length == 0)
            {
                ButtonAttribute buttonAttribute = (ButtonAttribute)methodInfo.GetCustomAttributes(typeof(ButtonAttribute), true)[0];
                string buttonText = string.IsNullOrEmpty(buttonAttribute.Text) ? methodInfo.Name : buttonAttribute.Text;

                if (GUILayout.Button(buttonText))
                {
                    methodInfo.Invoke(target, null);
                }
            }
            else
            {
                string warning = typeof(ButtonAttribute).Name + " works only on methods with no parameters";
                HelpBox_Layout(warning, MessageType.Warning, context: target, logToConsole: true);
            }
        }

        public static void NativeProperty_Layout(UnityEngine.Object target, PropertyInfo property)
        {
            object value = property.GetValue(target, null);

            if (value == null)
            {
                string warning = string.Format("{0} is null. {1} doesn't support reference types with null value", property.Name, typeof(ShowNativePropertyAttribute).Name);
                HelpBox_Layout(warning, MessageType.Warning, context: target);
            }
            else if (Field_Layout(value, property.Name) == false)
            {
                string warning = string.Format("{0} doesn't support {1} types", typeof(ShowNativePropertyAttribute).Name, property.PropertyType.Name);
                HelpBox_Layout(warning, MessageType.Warning, context: target);
            }
        }

        public static void NonSerializedField_Layout(UnityEngine.Object target, FieldInfo field)
        {
            object value = field.GetValue(target);

            if (value == null)
            {
                string warning = string.Format("{0} is null. {1} doesn't support reference types with null value", field.Name, typeof(ShowNonSerializedFieldAttribute).Name);
                HelpBox_Layout(warning, MessageType.Warning, context: target);
            }
            else if (Field_Layout(value, field.Name) == false)
            {
                string warning = string.Format("{0} doesn't support {1} types", typeof(ShowNonSerializedFieldAttribute).Name, field.FieldType.Name);
                HelpBox_Layout(warning, MessageType.Warning, context: target);
            }
        }

        public static void HorizontalLine(Rect rect, float height, Color color)
        {
            rect.height = height;
            EditorGUI.DrawRect(rect, color);
        }

        public static void HelpBox(Rect rect, string message, MessageType type, UnityEngine.Object context = null, bool logToConsole = false)
        {
            EditorGUI.HelpBox(rect, message, type);

            if (logToConsole)
            {
                DebugLogMessage(message, type, context);
            }
        }

        public static void HelpBox_Layout(string message, MessageType type, UnityEngine.Object context = null, bool logToConsole = false)
        {
            EditorGUILayout.HelpBox(message, type);

            if (logToConsole)
            {
                DebugLogMessage(message, type, context);
            }
        }

        public static bool Field_Layout(object value, string label)
        {
            GUI.enabled = false;
            bool isDrawn = true;

            switch (value)
            {
                case bool val:
                    EditorGUILayout.Toggle(label, val);
                    break;

                case int val:
                    EditorGUILayout.IntField(label, val);
                    break;

                case long val:
                    EditorGUILayout.LongField(label, val);
                    break;

                case float val:
                    EditorGUILayout.FloatField(label, val);
                    break;

                case double val:
                    EditorGUILayout.DoubleField(label, val);
                    break;

                case string val:
                    EditorGUILayout.TextField(label, val);
                    break;

                case Vector2 val:
                    EditorGUILayout.Vector2Field(label, val);
                    break;

                case Vector3 val:
                    EditorGUILayout.Vector3Field(label, val);
                    break;

                case Vector4 val:
                    EditorGUILayout.Vector4Field(label, val);
                    break;

                case Color val:
                    EditorGUILayout.ColorField(label, val);
                    break;

                case Bounds val:
                    EditorGUILayout.BoundsField(label, val);
                    break;

                case Rect val:
                    EditorGUILayout.RectField(label, val);
                    break;

                default:
                    var valueType = value.GetType();
                    if (typeof(UnityEngine.Object).IsAssignableFrom(valueType))
                    {
                        EditorGUILayout.ObjectField(label, (UnityEngine.Object)value, valueType, true);
                    }
                    else
                    {
                        isDrawn = false;
                    }
                    break;
            }

            GUI.enabled = true;
            return isDrawn;
        }

        private static void DebugLogMessage(string message, MessageType type, UnityEngine.Object context)
        {
            switch (type)
            {
                case MessageType.None:
                case MessageType.Info:
                    Debug.Log(message, context);
                    break;
                case MessageType.Warning:
                    Debug.LogWarning(message, context);
                    break;
                case MessageType.Error:
                    Debug.LogError(message, context);
                    break;
            }
        }
    }
}
