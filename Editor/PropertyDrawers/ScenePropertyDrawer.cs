﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    [CustomPropertyDrawer(typeof(SceneAttribute))]
    public class ScenePropertyDrawer : PropertyDrawerBase
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return 0f;
        }

        protected override void OnGUI_Internal(Rect rect, SerializedProperty property, GUIContent label)
        {
            if (property.propertyType == SerializedPropertyType.String)
            {
                GetStringScenes(property);
            }
            else if (property.propertyType == SerializedPropertyType.Integer)
            {
                GetIntScenes(property);
            }
            else
            {
                EditorGUILayout.HelpBox($"{property.type} is not supported by SceneAttribute\nUse [string] or [int] instead", MessageType.Warning);
            }
        }

        private void GetStringScenes(SerializedProperty property)
        {
            var sceneAttribute = PropertyUtility.GetAttribute<SceneAttribute>(property);
            List<string> allScenes = SceneUtilities.GetScenesInBuild(sceneAttribute.ShowPath);

            string propertyString = property.stringValue;

            int index = 0;
            for (int i = 0; i < allScenes.Count; i++)
            {
                if (allScenes[i].Contains(propertyString))
                {
                    index = i;
                    break;
                }
            }

            index = EditorGUILayout.Popup(property.displayName, index, allScenes.ToArray());

            if (index < 0 || index >= allScenes.Count)
            {
                index = 0;
            }

            property.stringValue = allScenes[index];
        }

        private void GetIntScenes(SerializedProperty property)
        {
            var sceneAttribute = PropertyUtility.GetAttribute<SceneAttribute>(property);
            List<string> allScenes = SceneUtilities.GetScenesInBuild(sceneAttribute.ShowPath);

            int propertyInt = property.intValue;

            if (propertyInt < 0 || propertyInt >= allScenes.Count)
            {
                propertyInt = 0;
            }

            propertyInt = EditorGUILayout.Popup(property.displayName, propertyInt, allScenes.ToArray());

            if (propertyInt < 0 || propertyInt >= allScenes.Count)
            {
                propertyInt = 0;
            }

            property.intValue = propertyInt;
        }

    }
}
